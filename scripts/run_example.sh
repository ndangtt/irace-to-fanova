# before running this example, please add fANOVA folder into PYTHONPATH
# example: export PYTHONPATH=$PYTHONPATH:/home/nguyen/Dropbox/fanova/fanova-master/

# without instance features
 Rscript convert.R --rdata ../example/irace.Rdata --paramFile ../example/params-01.txt --normalize method_2 --outDir ../example/output/

# with instance features
# Rscript convert.R --rdata ../example/irace.Rdata --normalize method_2 --outDir ../example/output/ --paramFeatureFile ../example/params-01-with-instance-features-01.txt --featureFile ../example/instance_features.txt
